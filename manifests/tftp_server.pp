class dnsmasq::tftp_server ( $dhcp_boot='pxelinux.0', $dhcp_dns_domain=$domain,
                             $dhcp_dns_search=$domain, $dhcp_router, $dhcp_dns_server,
                             $tftp_root='/var/lib/tftpboot', $tftp_secure=true
                           ) inherits dnsmasq {

  augeas {
    'dnsmasq_tftp_base':
      context => "/files/etc/dnsmasq.d/tftp",
      changes => [ "set dhcp-boot $dhcp_boot",
                   "set dhcp-option[1] option:domain-name,$dhcp_dns_domain",
                   "set dhcp-option[2] option:domain-search,$dhcp_dns_search",
                   "set dhcp-option[3] option:router,$dhcp_router",
                   "set dhcp-option[4] option:dns-server,$dhcp_dns_server",
                   "clear enable-tftp", "set tftp-root $tftp_root" ],
      require => File["/etc/dnsmasq.d"],
      notify  => Service[dnsmasq];
  }

  if $tftp_secure == true {
    augeas {
      'dnsmasq_tftp_secure':
        context => "/files/etc/dnsmasq.d/tftp",
        changes => [ "clear tftp-secure" ];
    }
  }
}
