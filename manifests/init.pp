# dnsmasq.pp - configure a small dns resolver and dhcp server
# Copyright (C) 2007 David Schmitt <david@schmitt.edv-bus.at>
# Copyright (C) 2010 Micah Anderson <micah@riseup.net>

class dnsmasq  ( $interface, $listen_address, $dhcp_range,
                 $dhcp_lease_time='12h', $dhcp_ntp_servers, $dhcp_domain=$domain,
                 $dhcp_authoritative=true
               ) {

  case $dnsmasq_ensure_version {
    '': { $dnsmasq_ensure_version = "present" }
  }

  package { 'dnsmasq':
    ensure => $dnsmasq_ensure_version,
    notify => Service[dnsmasq],
  }

  file {
    '/etc/dnsmasq.d':
      ensure   => directory,
      checksum => mtime,
      mode     => '0755',
      owner    => root,
      group    => root,
      require  => Package[dnsmasq],
      notify   => Service[dnsmasq];
  }

  augeas {
    'dnsmasq_dir_include':
      context => "/files/etc/dnsmasq.conf",
      changes => "set conf-dir /etc/dnsmasq.d",
      require => File["/etc/dnsmasq.conf"],
      notify  => Service[dnsmasq];

    'dnsmasq_puppet':
      context => "/files/etc/dnsmasq.d/puppet",
      changes => [ "set interface $interface", "clear bind-interfaces",
                   "set listen-address $listen_address",
                   "set dhcp-range $dhcp_range,$dhcp_lease_time",
                   "set dhcp-option[1] option:ntp-server,$dhcp_ntp_servers",
                   "set domain $dhcp_domain" ],
      require => File["/etc/dnsmasq.d"],
      notify  => Service[dnsmasq];
  }

  if $dhcp_authoritative == true {
    augeas { 'dhcp_authoritative':
      context => "/files/etc/dnsmasq.d/puppet",
      changes => "clear dhcp-authoritative",
      require => File["/etc/dnsmasq.d"],
      notify  => Service[dnsmasq];
    }
  }

  file { '/etc/dnsmasq.conf': checksum => md5, require => Package['dnsmasq'] }

  service { 'dnsmasq':
    ensure    => running,
    enable    => true,
    pattern   => '/usr/sbin/dnsmasq',
    subscribe => File['/etc/dnsmasq.conf']
  }
}
               
